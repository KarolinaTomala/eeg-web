var usersRef = firebase.database().ref("userinfo");
usersRef.on("value",snap=>fillUsers(snap));

var exams = Object();
document.addEventListener('users_obtained',function(e){
    var ev = new Event("change");
    document.getElementById('users').dispatchEvent(ev);
    initFlow(e.detail.user,e.detail.exam);
});

function manageFlow() {
    var user = document.getElementById('users').value;
    var exam = document.getElementById('exams-select').value;
    if(user === undefined || exam == undefined) return;
    initFlow(user,exam);
}

function initFlow(user,exam) {
    var eegDataRef = firebase.database().ref(`${user}/${exam}/EEG_data`);
    var deviceId = document.getElementById("DeviceID");
    var deviceIdRef = firebase.database().ref().child(`${user}/${exam}/Device_id`);
    deviceIdRef.on('value', snap => deviceId.innerHTML = snap.val());

    for(var i=1;i<=4;i++) {
        eegDataRef.child(`eeg${i}`).on('value',snap => onValue(snap));
    }
}

var obj = Object();
var charts = Array();

function onValue(snap) {

    obj[snap.key] = Array();
    var values = snap.val().filter(el => el !="0"); //filter out zeros - result of poor connection
    for(var i=0;i<values.length;i++) {
        obj[snap.key].push({x:i,y:parseFloat(values[i])});
    }
    // document.getElementById(snap.key).innerHTML = obj[snap.key].y;
    drawChart(snap.key);
}


function createCanvas(key) {

    var chartContainer = document.getElementById('charts');
    var canvas = document.createElement('canvas');

    canvas.setAttribute('width','60vw');
    canvas.setAttribute('height','30vh');
    canvas.setAttribute('position','relative');
    canvas.setAttribute('id',`canvas-${key}`);

    chartContainer.appendChild(canvas);
    var id = document.getElementById(`canvas-${key}`);
    return id;
}

function drawChart(key) {

    var id = document.getElementById(key);

    if(id) {
        id.parentNode.removeChild(key);
    }

    var ctx = createCanvas(key);
    charts.push(new Chart(ctx, {
            type: 'line',
            data: {
                datasets:[{
                    label:"",
                    data:obj[key],
                    pointRadius: 0,
                    fill: false,
                    borderColor: "rgba(180,154,46,0.94)",
                    lineTension: 0.05
                }]
            },
            options:{
                events:[],
                responsive: true,
                legend:{
                    display: false
                },
                plugins: {
                    zoom: {

                        zoom: {
                            drag: true,
                            speed: 0.05,
                            enabled: true,
                            mode: 'xy',
                            sensitivity: 1,
                        }
                    }
                },
                title:{
                    fontColor: 'black',
                    fontSize: 17,
                    fontStyle: 'bold',
                    display:true,
                    text:key.toUpperCase()
                },
                scales: {
                    xAxes: [{
                        type: 'linear', // MANDATORY TO SHOW YOUR POINTS! (THIS IS THE IMPORTANT BIT)
                        display: true, // mandatory
                        scaleLabel: {
                            display: true, // mandatory
                            fontColor:"black",
                            labelString:"time [s x 10^-1]" // optional

                        },
                    }],
                    yAxes: [{ // and your y axis customization as you see fit...
                        display: true,
                        scaleLabel: {
                            display: true,
                            fontColor:"black",
                            labelString: "electric potential [uV]"
                        }
                    }],
                }
            }
        }
    ));
}

function fillUsers(snap) {

    document.getElementById('charts').style.display = "block";
    document.getElementById('notAllowedMessage').style.display = "none";

    var select = document.getElementById("users");
    var values = snap.val();

    if(values === undefined) {
      return;
    }

    for(user in values) {

        createOption("users",user);
        exams[user] = Array();

        for(exam in values[user]) {
            exams[user].push(exam);
        }
    }

    select.addEventListener("change",(event)=>{fillExams(event)});
    document.dispatchEvent(new CustomEvent('users_obtained',{
        detail:{
            exam: Object.keys(values[Object.keys(values)[0]])[0],
            user: Object.keys(values)[0]
        }
    }));
}

function createOption(where,what) {

    var opt = document.createElement('option');
    opt.setAttribute("id",`${where}-${what}`);
    opt.innerHTML = what;
    document.getElementById(where).appendChild(opt);
}

function clear(what) {

    var ref = document.getElementById(what);
    ref.innerHTML = "";
}

function fillExams(ev) {

    var currentUser = ev.target.value;
    clear('exams-select');

    for(exam in exams[currentUser]) {
        createOption('exams-select',exams[currentUser][exam]);
    }

    document.getElementById('exams-select').style.display = "block";
}
