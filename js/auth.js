function login() {
    var email = document.getElementById("email_field").value;
    var password = document.getElementById("password_field").value;



    firebase.auth().signInWithEmailAndPassword(email, password).then(function(user) {
  
        window.location = "./eeg-web.html";

    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
            alert('Wrong password.');
        } else {
            alert(errorMessage);
        }
        console.log(error);
    });

}


function logout() {

    firebase.auth().signOut().then(function() {
        window.alert("You've successfully signed out");
        window.location="./login.html";
    }).catch(function(error) {
        window.alert(error.message);
    });
}
